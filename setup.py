from setuptools import setup

setup(
    name='a4dfmri',
    version='0.0.0',
    packages=['a4dfmri'],
    scripts=['scripts/a4dfmri'],
    url='https://gitlab.inria.fr/cobcom/a4dfmri',
    license='',
    author='Isa Costantini',
    author_email='',
    description='A Python package that implements the A4D fmri algorithm.',
    install_requires=[
        'numpy>=1.22',
        'nibabel>=3.2.2',
        'scipy>=1.7.3',
    ]
)
