#!/usr/bin/env python

import argparse
import logging

import nibabel as nib
import numpy as np

import a4dfmri
from a4dfmri.filters.hrf_operators import hrf_operator_TR_adjusted as hrf_op


def parse_arguments():

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'image',
        help='The filename of the image to filter. Can be of any format '
             'supported by nibabel.')
    parser.add_argument(
        'output', type=str,
        help='The filename of the output image. Can be of any format supported '
             'by nibabel.')
    parser.add_argument(
        '--nb-iterations', type=float, default=40,
        help='The number of iterations to perform. Default is 40.')
    parser.add_argument(
        '--repetition-time', type=float, metavar='TR',
        help='The repetition time of the data. If not provided, it will be '
             'obtained from the image header.')
    parser.add_argument(
        '--verbose', action='store_true',
        help='If present, increases the verbosity of the output.')

    return parser.parse_args()


def main():

    args = parse_arguments()

    # Set the verbosity level.
    if args.verbose:
        logging.basicConfig(level=logging.INFO)

    # Load the image.
    logging.info(f'Loading the image data from {args.image}.')
    nii = nib.load(args.image)
    image = np.array(nii.get_fdata(), dtype=np.float32)

    # Find the TR from the metadata if it was not provided.
    if args.repetition_time is None:
        args.repetition_time = nii.header.get_zooms()[-1]
        logging.warning(
            f'Using a repetition time of {args.repetition_time:0.3f} seconds.')

    # Compute the HRF operator
    t_size = np.size(image, 3)
    dirac = np.zeros((t_size,), )
    dirac[t_size // 2 + 1] = 1
    hrf = hrf_op(dirac, args.repetition_time)

    logging.info('Starting denoising.')
    denoised_image = a4dfmri.denoise(image, hrf, n_iterations=args.nb_iterations)

    nib.save(nib.Nifti1Image(denoised_image, nii.affine), args.output)


if __name__ == '__main__':
    main()
