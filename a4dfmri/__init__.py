import warnings

import numpy as np
from scipy.ndimage.filters import convolve1d

try:
    import a4dfmri.tensorflow as _implementation
except ImportError:
    warnings.warn('Could not load tensorflow implementation, using numpy.')
    import a4dfmri.numpy as _implementation


def denoise(
        image: np.ndarray,
        hemodynamic_response=None,
        n_iterations: int = 1,
        regularization_weight: float = 0.5,
        step_size: float = 0.01) -> np.ndarray:

    # If the hemodynamic response is not provided, use the identity.
    if hemodynamic_response is None:
        hemodynamic_response = np.ones((1,))

    # Denoise the image iteratively using the available implementation.
    denoised_image = image.copy()
    for i in range(n_iterations):
        regularization = _implementation.denoise(denoised_image)
        data_fit = convolve1d(
            (image - convolve1d(denoised_image, hemodynamic_response, axis=-1)),
            hemodynamic_response[::-1], axis=-1)

        change = regularization_weight * regularization + \
                 (1.0 - regularization_weight) * data_fit
        denoised_image += step_size * change

    return denoised_image
