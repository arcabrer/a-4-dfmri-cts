import logging
from itertools import product
from time import time

import numpy as np
import tensorflow as tf
from scipy.ndimage.filters import gaussian_filter


def compute_gradients(image):
    return np.array(
        [np.diff(image, append=0, axis=i) for i in range(image.ndim)])


def gaussian(x):
    return np.exp(-x ** 2 / (2 * 0.1 ** 2))


def denoise(image):

    logging.info('Computing image gradients.')
    tic = time()
    gradients = compute_gradients(image)
    logging.info(f'Computed gradients in {time() - tic} seconds.')

    logging.info('Computing structure tensor for each voxel.')
    tic = time()
    ndim = image.ndim
    structure_tensor = np.zeros(image.shape + (ndim, ndim), dtype=np.float32)
    for i, j in product(range(ndim), repeat=2):
        if j > i:
            continue
        structure_tensor[..., i, j] = gaussian_filter(
            gradients[i] * gradients[j], 1)
    logging.info(f'Computed structure tensor in {time() - tic} seconds.')

    logging.info('Decomposition of the structure tensor.')
    tic = time()
    eigenvalues = np.empty(image.shape + (image.ndim,), dtype=np.float32)
    eigenvectors = np.empty(image.shape + (image.ndim, image.ndim), dtype=np.float32)
    for sample in range(image.shape[3]):
        eigenvalues[:, :, :, sample, :], eigenvectors[:, :, :, sample, :, :] =\
            tf.linalg.eigh(structure_tensor[:, :, :, sample, :, :])
    logging.info(f'Decomposed structure tensor in {time() - tic} seconds.')

    # Set the maximum eigenvalue of each voxel using a Gaussian and all others
    # to 1. Because we use eigh instead of eig, they eigenvalues are in
    # ascending order.
    logging.info('Rebuilding structure tensor.')
    tic = time()
    new_eigenvalues = np.ones_like(eigenvalues)
    new_eigenvalues[..., -1] = gaussian(
        eigenvalues[..., -1] / np.max(eigenvalues))
    weighted_eigenvectors = eigenvectors * new_eigenvalues[..., None, :]
    new_structure_tensor = np.matmul(
        weighted_eigenvectors,
        np.transpose(eigenvectors, (0, 1, 2, 3, 5, 4)))
    logging.info(f'Rebuilt structure tensor in {time() - tic} seconds.')

    logging.info('Computing the oriented gradients.')
    tic = time()
    oriented_gradient = np.matmul(
        new_structure_tensor,
        gradients.transpose((1, 2, 3, 4, 0))[..., None])
    oriented_gradient = np.squeeze(oriented_gradient, axis=-1)
    logging.info(f'Computed oriented gradients in {time() - tic} seconds.')

    logging.info('Computing divergence of oriented gradients.')
    divergence = np.sum(
        [np.diff(oriented_gradient[..., i], prepend=0, axis=i)
         for i in range(ndim)],
        axis=0)
    logging.info(f'Computed divergence of oriented gradients in '
                 f'{time() - tic} seconds.')

    return divergence
